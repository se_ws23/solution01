ThisBuild / version := "1.0.0-SNAPSHOT"

ThisBuild / scalaVersion := "3.3.1"

lazy val root = (project in file("."))
  .settings(
    name := "arithmetic"
  )

ThisBuild / libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.17" % Test