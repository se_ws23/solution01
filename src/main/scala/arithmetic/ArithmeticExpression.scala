package arithmetic

enum ArithmeticExpression:
  case Num(z: Int)
  case Minus(e: ArithmeticExpression)
  case Plus(e1: ArithmeticExpression, e2: ArithmeticExpression)
  case Mult(e1: ArithmeticExpression, e2: ArithmeticExpression)
  case Div(e1: ArithmeticExpression, e2: ArithmeticExpression)
  case Pow(e1: ArithmeticExpression, e2: ArithmeticExpression)

object ArithmeticExpression:
  def evaluate(expression: ArithmeticExpression): Double = expression match
    case Num(z) => z
    case Minus(e) => -evaluate(e)
    case Plus(e1, e2) => evaluate(e1)+evaluate(e2)
    case Mult(e1, e2) => evaluate(e1)*evaluate(e2)
    case Div(e1, e2) => evaluate(e1)/evaluate(e2)
    case Pow(e1, Num(0)) => 1
    case Pow(e1, Num(z)) if z < 0 => evaluate(Mult(Div(Num(1),e1), Pow(e1, Num(z+1))))
    case Pow(e1, Num(z)) if z > 0 => evaluate(Mult(e1, Pow(e1, Num(z-1))))
    case Pow(e1, e2) => evaluate(Pow(e1, Num(evaluate(e2).toInt)))

  def pretty(expression: ArithmeticExpression): String = expression match
    case Num(z) => z.toString
    case Minus(e) => s"-${pretty(e)}"
    case Plus(e1, e2) => s"(${pretty(e1)}+${pretty(e2)})"
    case Mult(e1, e2) => s"${pretty(e1)}*${pretty(e2)}"
    case Div(e1, e2) => s"${pretty(e1)}/${pretty(e2)}"
    case Pow(e1@Num(_), e2@Num(_)) => s"${pretty(e1)}^${pretty(e2)}"
    case Pow(e1, e2) => s"(${pretty(e1)})^(${pretty(e2)})"

  def evaluate(expressions: List[ArithmeticExpression]): List[Double] = expressions.map(evaluate)

  def showResults(expressions: List[ArithmeticExpression]): String =
    expressions.foldRight("") ((e, s) => s"${pretty(e)} = ${evaluate(e)}\n$s").dropRight(1)