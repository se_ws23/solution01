package arithmetic

import org.scalatest.funsuite.AnyFunSuite
import ArithmeticExpression._

class ArithmeticExpressionTest extends AnyFunSuite {
  test("evaluate 2/0") {
    val expression = Div(Num(2), Num(0))
    assert(evaluate(expression) == Double.PositiveInfinity)
  }

  test("evaluate 2^10") {
    val expression = Pow(Num(2), Num(10))
    assert(evaluate(expression) == 1024)
  }

  test("evaluate 2/0, 2^10") {
    val expressions = List(Div(Num(2), Num(0)), Pow(Num(2), Num(10)))
    assert(evaluate(expressions) == List(Double.PositiveInfinity, 1024))
  }

  test("prettier (2/(7+1))*-1") {
    val expression = Mult(Div(Num(2),Plus(Num(7), Num(1))),Minus(Num(1)))
    assert(pretty(expression) == "2/(7+1)*-1")
  }

  test("prettier (5^(3*4)") {
    val expression = Pow(Num(5), Mult(Num(3),Num(4)))
    assert(pretty(expression) == "(5)^(3*4)")
  }

  test("results of (2/(7+1))*-1, (5^(3*4)") {
    val expressions = List(
      Mult(Div(Num(2),Plus(Num(7), Num(1))),Minus(Num(1))),
      Pow(Num(5), Mult(Num(3),Num(4)))
    )
    assert(showResults(expressions) == "2/(7+1)*-1 = -0.25\n(5)^(3*4) = 2.44140625E8")
  }
}
